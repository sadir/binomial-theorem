const binomialTheorem = (() => {
    const _pascalsTriangle = nthRow => {
        const row = [];
        let numerator = nthRow;
        let denominator = 1;

        for (let i = 0; i <= Math.round(nthRow / 2); i++) {
            let nCk;

            if (i === 0) {
                nCk = 1;
            } else if (i === 1) {
                nCk = nthRow;
            } else {
                numerator *= nthRow - i + 1;
                denominator *= i;
                nCk = numerator / denominator;
            }

            row[i] = nCk;
            row[nthRow - i] = nCk;
        }

        return row;
    };

    return n => {
        if (n === 0) {
            return "1";
        }

        const coefficients = _pascalsTriangle(n);
        const addends = [];

        const beautify = (coefficient, k) => {
            let result = "";

            const beautifyVariable = (variable, power) => {
                if (power === 0) {
                    return "";
                }

                if (power === 1) {
                    return variable;
                }

                return variable + `<sup>${power}</sup>`;
            };

            if (coefficient > 1) {
                result += coefficient;
            }

            result += beautifyVariable("a", n - k);
            result += beautifyVariable("b", k);

            return result;
        };

        for (let i = 0; i < coefficients.length; i++) {
            addends.push(beautify(coefficients[i], i));
        }

        return addends.join(" + ");
    };
})();